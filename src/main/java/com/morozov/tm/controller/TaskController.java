package com.morozov.tm.controller;

import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.api.service.ITaskService;
import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.exception.TaskNotFoundException;
import com.morozov.tm.model.dto.ProjectDto;
import com.morozov.tm.model.dto.TaskDto;
import com.morozov.tm.util.DateFormatUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/tasks")
public class TaskController {
    @Autowired
    private ITaskService taskService;
    @Autowired
    private IProjectService projectService;

    @RequestMapping(value = "")
    public String taskList(Model model) throws ProjectNotFoundException {
        final @NotNull List<TaskDto> taskList = taskService.findAllTask();
        Map<String, String> projectIdNameMap = new HashMap<>();
        for (TaskDto taskDto: taskList) {
            @NotNull final String idProject = taskDto.getProjectId();
            @NotNull final String projectName = projectService.findById(idProject).getName();
            projectIdNameMap.put(idProject,projectName);
        }
        model.addAttribute("tasklist", taskList);
        model.addAttribute("projectNameMap", projectIdNameMap);
        return "task/taskList";
    }

    @RequestMapping(value = "/add")
    public String addTask(Model model) {
        @NotNull final List<ProjectDto> projectDtoList = projectService.findAllProject();
        model.addAttribute("projectList", projectDtoList);
        return "task/taskCreate";
    }

    @PostMapping(value = "/create")
    public String createTask(
            @RequestParam("taskName") String taskName,
            @RequestParam("projectId") String projectId
    ) throws ProjectNotFoundException {
        taskService.addTask(taskName, projectId);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/view/{taskId}")
    public String taskView(@PathVariable("taskId") String id, Model model) throws TaskNotFoundException {
        @NotNull final TaskDto task = taskService.findById(id);
        model.addAttribute("task", task);
        return "task/taskView";
    }

    @RequestMapping(value = "/edit/{taskId}")
    public String taskEdit(@PathVariable("taskId") String id, Model model) throws TaskNotFoundException {
        @NotNull final TaskDto task = taskService.findById(id);
        @Nullable final String dataBegin = DateFormatUtil.formattedDataToString(task.getStartDate());
        @Nullable final String dataEnd = DateFormatUtil.formattedDataToString(task.getEndDate());
        @Nullable final String dataCreate = DateFormatUtil.formattedDataToString(task.getCreatedData());
        model.addAttribute("task", task);
        model.addAttribute("dataCreate", dataCreate);
        model.addAttribute("dataBegin", dataBegin);
        model.addAttribute("dataEnd", dataEnd);
        return "task/taskEdit";
    }

    @PostMapping(value = "/save")
    public String taskUpdate(
            @RequestParam("id") String taskId,
            @RequestParam("name") String taskName,
            @RequestParam("description") String taskDescription,
            @RequestParam("dataBegin") String dataBegin,
            @RequestParam("dataEnd") String dataEnd,
            @RequestParam("projectId") String projectId
    ) throws ParseException, ProjectNotFoundException, TaskNotFoundException {
        taskService.updateTask(taskId, taskName, taskDescription, dataBegin, dataEnd, projectId);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/delete/{taskId}")
    public String deleteTask(
            @PathVariable("taskId") String id
    ) throws TaskNotFoundException {
        taskService.deleteTaskById(id);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/deleteAll")
    public String deleteAllTask() {
        taskService.deleteAllTask();
        return "redirect:/tasks";
    }

}
