package com.morozov.tm.controller;

import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.api.service.ITaskService;
import com.morozov.tm.model.dto.ProjectDto;
import com.morozov.tm.model.dto.TaskDto;
import com.morozov.tm.service.ProjectService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping(value = "/main")
public class MainController {
    @Autowired
    private IProjectService projectService;
    @Autowired
    private ITaskService taskService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView main() {
        return new ModelAndView("main");
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(
            @RequestParam("searchString") String searchString,
            Model model
    ) {
        @NotNull final List<ProjectDto> projectDtoList = projectService.searchByString(searchString);
        @NotNull final List<TaskDto> taskDtoList = taskService.searchByString(searchString);
        model.addAttribute("projectlist", projectDtoList);
        model.addAttribute("tasklist", taskDtoList);
        model.addAttribute("searchString",searchString);
        return "search/search";
    }
}
