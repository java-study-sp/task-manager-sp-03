package com.morozov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatUtil {
    @NotNull
    static final String DATA_FORMAT = "yyyy-MM-dd";

    public static Date formattedData(@NotNull final String data) throws ParseException {
        if (data.isEmpty()) return null;
        @NotNull final SimpleDateFormat format = new SimpleDateFormat(DATA_FORMAT);
        @Nullable final Date resultDate = format.parse(data);
        return resultDate;
    }
    public static String formattedDataToString(Date date) {
        if(date == null) return "не установлена";
        @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat(DATA_FORMAT);
        return dateFormat.format(date);
    }
}
