package com.morozov.tm.api.repository;

import com.morozov.tm.model.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {

    @NotNull
    @Query("SELECT p FROM Project p WHERE p.name LIKE CONCAT('%',?1,'%') OR p.description LIKE CONCAT('%',?1,'%')")
    List<Project> searchByString(@NotNull String string);

}
