package com.morozov.tm.api.repository;

import com.morozov.tm.model.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {
    @NotNull
    @Query("SELECT t FROM Task t WHERE t.project.id = ?1")
    List<Task> findAllTaskByProjectId(@NotNull String projectId);

    @NotNull
    @Query("SELECT t FROM Task t WHERE t.name LIKE CONCAT ('%',?1,'%') OR t.description LIKE CONCAT ('%',?1,'%')")
    List<Task> searchByString(@NotNull String string);

}
