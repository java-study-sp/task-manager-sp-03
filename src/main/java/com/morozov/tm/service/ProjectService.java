package com.morozov.tm.service;

import com.morozov.tm.api.repository.IProjectRepository;
import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.model.dto.ProjectDto;
import com.morozov.tm.model.entity.Project;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.util.DateFormatUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService implements IProjectService {
    @Autowired
    private IProjectRepository projectRepository;


    @NotNull
    @Override
    public ProjectDto findById(String id) throws ProjectNotFoundException {
        @NotNull final Project project = projectRepository.findById(id).orElseThrow(ProjectNotFoundException::new);
        @NotNull ProjectDto projectDto = transferProjectToProjectDto(project);
        return projectDto;
    }

    @Override
    public @NotNull List<ProjectDto> findAllProject() {
        @NotNull final List<Project> projectList = projectRepository.findAll();
        @NotNull final List<ProjectDto> projectDtoList = transferListProjectToListProjectDto(projectList);
        return projectDtoList;
    }

    @NotNull
    @Override
    public ProjectDto addProject(@NotNull final String projectName) {
        @NotNull final Project project = new Project();
        project.setName(projectName);
        return transferProjectToProjectDto(projectRepository.save(project));
    }


    @Override
    public void deleteProjectById(@NotNull String id) throws ProjectNotFoundException {
        projectRepository.findById(id).orElseThrow(ProjectNotFoundException::new);
        projectRepository.deleteById(id);
    }


    @Override
    public ProjectDto updateProject(
            @NotNull final String id, @NotNull final String projectName, @NotNull final String projectDescription,
            @NotNull final String dataStart, @NotNull final String dataEnd)
            throws ParseException, ProjectNotFoundException {
        @NotNull final Project project = projectRepository.findById(id).orElseThrow(ProjectNotFoundException::new);
        project.setId(id);
        project.setName(projectName);
        project.setDescription(projectDescription);
        final Date updatedStartDate = DateFormatUtil.formattedData(dataStart);
        if (updatedStartDate != null) {
            project.setStatus(StatusEnum.PROGRESS);
        }
        project.setStartDate(updatedStartDate);
        final Date updatedEndDate = DateFormatUtil.formattedData(dataEnd);
        if (updatedEndDate != null) {
            project.setStatus(StatusEnum.READY);
        }
        project.setEndDate(updatedEndDate);
        projectRepository.save(project);
        return  transferProjectToProjectDto(project);
    }

    @Override
    public final List<ProjectDto> searchByString(@NotNull final String string) {
        if (string.isEmpty()) {
            return new ArrayList<>();
        }
        @NotNull final List<Project> projectListByUserId = projectRepository.searchByString(string);
        return transferListProjectToListProjectDto(projectListByUserId);
    }

    @Override
    public void deleteAll() {
        projectRepository.deleteAll();
    }

    @NotNull
    @Override
    public ProjectDto transferProjectToProjectDto(@NotNull final Project project) {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        //projectDto.setUserId(project.getUser().getId());
        projectDto.setCreatedData(project.getCreatedData());
        projectDto.setStartDate(project.getStartDate());
        projectDto.setEndDate(project.getEndDate());
        projectDto.setStatus(project.getStatus());
        return projectDto;
    }

    @NotNull
    @Override
    public Project transferProjectDtoToProject(@NotNull final ProjectDto projectDto) {
        @NotNull final Project project = new Project();
        project.setId(projectDto.getId());
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        //project.setUser(user);
        project.setCreatedData(projectDto.getCreatedData());
        project.setStartDate(projectDto.getStartDate());
        project.setEndDate(projectDto.getEndDate());
        project.setStatus(projectDto.getStatus());
        return project;
    }

    @NotNull
    @Override
    public List<ProjectDto> transferListProjectToListProjectDto(@NotNull List<Project> projectList) {
        @NotNull final List<ProjectDto> projectDtoList = new ArrayList<>();
        for (@NotNull final Project project : projectList) {
            projectDtoList.add(transferProjectToProjectDto(project));
        }
        return projectDtoList;
    }
}
