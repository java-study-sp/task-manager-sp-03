package com.morozov.tm.model.entity;

import com.morozov.tm.enumerated.StatusEnum;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;
@MappedSuperclass
public class AbstractWorkEntity extends AbstractEntity {
    @NotNull
    @Column(nullable = false)
    private String name = "";
    @NotNull
    @Column(name = "date_create", nullable = false)
    private Date createdData = new Date();
    @NotNull
    @Column(nullable = false)
    private String userId = "";
    @NotNull
    private String description = "";
    @Nullable
    @Column(name = "date_start")
    private Date startDate = null;
    @Nullable
    @Column(name = "date_end")
    private Date endDate = null;
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private StatusEnum status = StatusEnum.PLANNED;

    public AbstractWorkEntity() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedData() {
        return createdData;
    }

    public void setCreatedData(Date createdData) {
        this.createdData = createdData;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }
}
