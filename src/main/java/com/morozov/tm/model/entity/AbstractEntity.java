package com.morozov.tm.model.entity;


import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;
@MappedSuperclass
public class AbstractEntity implements Serializable {
    @NotNull
    @Id
    private String id = UUID.randomUUID().toString();

    public AbstractEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
