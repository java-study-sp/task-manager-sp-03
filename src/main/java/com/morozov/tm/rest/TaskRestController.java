package com.morozov.tm.rest;

import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.api.service.ITaskService;
import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.exception.TaskNotFoundException;
import com.morozov.tm.model.dto.TaskDto;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping(value = "/resttasks")
public class TaskRestController {
    @Autowired
    private ITaskService taskService;
    @Autowired
    private IProjectService projectService;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<TaskDto> projectDtoList() {
        return taskService.findAllTask();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/find/{id}", produces = "application/json")
    @ResponseBody
    public TaskDto findById(@PathVariable String id) throws TaskNotFoundException {
        return taskService.findById(id);
    }

    @PostMapping(value = "/create", produces = "application/json")
    @ResponseBody
    public TaskDto createTask(@RequestParam("taskName") String taskName,
                              @RequestParam("projectId") String projectId) throws ProjectNotFoundException {
        return taskService.addTask(taskName, projectId);
    }

    @PostMapping(value = "/update", produces = "application/json")
    @ResponseBody
    public TaskDto updateTask(@RequestParam("id") String taskId,
                              @RequestParam("projectId") String projectId,
                              @RequestParam("name") String taskName,
                              @RequestParam("description") String taskDescription,
                              @RequestParam("dataBegin") String dataBegin,
                              @RequestParam("dataEnd") String dataEnd,
                              HttpServletResponse response) throws ParseException, ProjectNotFoundException, TaskNotFoundException {
        @NotNull final TaskDto taskDto = taskService.updateTask(taskId, taskName, taskDescription, dataBegin, dataEnd, projectId);
        response.setHeader("Location", ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/tasks/view/" + taskDto.getId()).toUriString());
        return taskDto;
    }
    @PostMapping(value = "/delete")
    public void deleteTask(@RequestParam("id") String taskId) throws TaskNotFoundException {
       taskService.deleteTaskById(taskId);
    }
}
