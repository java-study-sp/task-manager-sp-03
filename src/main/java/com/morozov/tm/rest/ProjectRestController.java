package com.morozov.tm.rest;

import com.morozov.tm.api.service.IProjectService;
import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.model.dto.ProjectDto;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping(value = "/restprojects")
public class ProjectRestController {
    @Autowired
    private IProjectService projectService;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<ProjectDto> projectDtoList() {
        return projectService.findAllProject();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/find/{id}", produces = "application/json")
    @ResponseBody
    public ProjectDto findById(@PathVariable String id) throws ProjectNotFoundException {
        return projectService.findById(id);
    }

    @PostMapping(value = "/create", produces = "application/json")
    @ResponseBody
    public ProjectDto createProject(@RequestParam("projectName") String projectName) {
        return projectService.addProject(projectName);
    }

    @PostMapping(value = "/update", produces = "application/json")
    @ResponseBody
    public ProjectDto updateProject(@RequestParam("id") String projectId,
                                    @RequestParam("name") String projectName,
                                    @RequestParam("description") String projectDescription,
                                    @RequestParam("dataBegin") String dataBegin,
                                    @RequestParam("dataEnd") String dataEnd,
                                    HttpServletResponse response) throws ParseException, ProjectNotFoundException {
        @NotNull final ProjectDto projectDto = projectService.updateProject(projectId, projectName, projectDescription, dataBegin, dataEnd);
        response.setHeader("Location", ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/projects/view/" + projectDto.getId()).toUriString());
        return projectDto;
    }

    @PostMapping(value = "/delete")
    public void deleteProject(@RequestParam("id") String projectId) throws ProjectNotFoundException {
        projectService.deleteProjectById(projectId);
    }
}
