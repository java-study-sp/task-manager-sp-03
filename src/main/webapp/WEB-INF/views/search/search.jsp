<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 02.12.2019
  Time: 18:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<%@include file="/WEB-INF/views/jspf/navbar.jspf" %>
<div class="container-fluid">
    <c:choose>
        <c:when test="${!projectlist.isEmpty() || !tasklist.isEmpty()}">
            <div class="mt-1 ml-3">
                <h1>Search result by string "${searchString}"</h1>
            </div>
            <c:if test="${!projectlist.isEmpty()}">
                <h1>Project list</h1>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Data Create</th>
                        <th scope="col">Data Begin</th>
                        <th scope="col">Data End</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${projectlist}" var="project">
                        <tr>
                            <td scope="row">${project.id}</td>
                            <td scope="row">${project.name}</td>
                            <td scope="row">${project.description}</td>
                            <td scope="row">${project.createdData}</td>
                            <td scope="row">${project.startDate}</td>
                            <td scope="row">${project.endDate}</td>
                            <td scope="row">${project.status}</td>
                            <td scope="row"><a href="/projects/view/${project.id}/">VIEW</a></td>
                            <td scope="row"><a href="/projects/edit/${project.id}/">EDIT</a></td>
                            <td scope="row"><a href="/projects/remove/${project.id}/">REMOVE</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${!tasklist.isEmpty()}">
                <h1>Task list</h1>
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Project ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Data Create</th>
                        <th scope="col">Data Begin</th>
                        <th scope="col">Data End</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${tasklist}" var="project">
                        <tr>
                            <td scope="row">${project.id}</td>
                            <td scope="row">${project.idProject}</td>
                            <td scope="row">${project.name}</td>
                            <td scope="row">${project.description}</td>
                            <td scope="row">${project.createdData}</td>
                            <td scope="row">${project.startDate}</td>
                            <td scope="row">${project.endDate}</td>
                            <td scope="row">${project.status}</td>
                            <td scope="row"><a href="/tasks/view/${project.id}/">VIEW</a></td>
                            <td scope="row"><a href="/tasks/edit/${project.id}/">EDIT</a></td>
                            <td scope="row"><a href="/tasks/delete/${project.id}/">REMOVE</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </c:when>
        <c:otherwise>
            <div class="mt-1 ml-3">
                <h1>Search result by string "${searchString}" didn't return results</h1>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
