<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2019
  Time: 14:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project Create</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<%@include file="/WEB-INF/views/jspf/navbar.jspf"%>
<div class="container">
    <div class="mt-1 ml-3">
        <h1>Project Create</h1>
    </div>
    <div class="mt-1 ml-3">
        <form action="/projects/create" method="post">
            <div class="form-group col-6">
                <label for="formGroupExampleInput">Enter project Name</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Project name"
                       name="projectName">
            </div>
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
    </div>
</div>
</body>
</html>
