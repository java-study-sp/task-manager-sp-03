<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"
         pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Project List</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<%@include file="/WEB-INF/views/jspf/navbar.jspf"%>
<div class="container-fluid">
    <div>
        <h1>Project List</h1>
    </div>
    <div>
        <c:choose>
            <c:when test="${!projectlist.isEmpty()}">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Data Create</th>
                        <th scope="col">Data Begin</th>
                        <th scope="col">Data End</th>
                        <th scope="col">Status</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${projectlist}" var="project" varStatus="loop">
                        <tr>
                            <td scope="row">${loop.count}</td>
                            <td scope="row">${project.name}</td>
                            <td scope="row">${project.description}</td>
                            <td scope="row">${project.createdData}</td>
                            <td scope="row">${project.startDate}</td>
                            <td scope="row">${project.endDate}</td>
                            <td scope="row">${project.status}</td>
                            <td scope="row"><a href="/projects/view/${project.id}/">VIEW</a></td>
                            <td scope="row"><a href="/projects/edit/${project.id}/">EDIT</a></td>
                            <td scope="row"><a href="/projects/remove/${project.id}/">REMOVE</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <form action="/projects/deleteAll" method="get">
                    <button type="submit" class="btn btn-primary">Delete all projects</button>
                </form>
            </c:when>
            <c:otherwise>
                <h3>Project list is empty</h3>
            </c:otherwise>
        </c:choose>
        <form action="/projects/add" method="get">
            <button type="submit" class="btn btn-primary">Create Project</button>
        </form>
    </div>
</div>
</body>
</html>
