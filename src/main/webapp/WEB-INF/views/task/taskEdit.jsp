<%--
  Created by IntelliJ IDEA.
  User: Андрей
  Date: 29.11.2019
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task Edit</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<%@include file="/WEB-INF/views/jspf/navbar.jspf"%>
<div class="container-fluid">
    <div class="mt-1 ml-3">
        <h1>Task edit</h1>
    </div>
    <div class="mt-1 ml-3">
        <form action="/tasks/save" method="post">
            <div class="form-group col-6">
                <label for="formGroupExampleInput">Task ID</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Task ID"
                       name="id" value="${task.id}" readonly>
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput0">Project ID</label>
                <input type="text" class="form-control" id="formGroupExampleInput0" placeholder="Project ID"
                       name="projectId" value="${task.projectId}" readonly>
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput1">Task name</label>
                <input type="text" class="form-control" id="formGroupExampleInput1" placeholder="Task name"
                       name="name" value="${task.name}">
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput2">Task description</label>
                <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Task description"
                       name="description" value="${task.description}">
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput3">Task Date Create</label>
                <input type="text" class="form-control" id="formGroupExampleInput3" placeholder="Task Date Create"
                       name="dataCreate" value="${dataCreate}" readonly>
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput4">Task Date Begin</label>
                <input type="date" class="form-control" id="formGroupExampleInput4" placeholder="Task Date Begin"
                       name="dataBegin" value="${dataBegin}">
            </div>
            <div class="form-group col-6">
                <label for="formGroupExampleInput5">Task Date End</label>
                <input type="date" class="form-control" id="formGroupExampleInput5" placeholder="Task Date End"
                       name="dataEnd" value="${dataEnd}">
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>
</body>
</html>
