package com.morozov.tm.rest.project;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProjectCreateRestTest {
    @NotNull private static final String PROJECT_CREATE_REST_URL = "http://localhost:8080/restprojects/create";
    @NotNull private static final String PROJECT_DELETE_REST_URL = "http://localhost:8080/restprojects/delete";
    @NotNull private final ObjectMapper objectMapper = new ObjectMapper();
    @NotNull private final String projectName = "Test Project";
    @Nullable private String projectResultAsJson;
    @Nullable private String projectId;

    @Before
    public void setUp() throws Exception {
        @NotNull final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("projectName", projectName);
        @NotNull final HttpEntity<?> request = new HttpEntity<Object>(requestBody, header);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        projectResultAsJson = restTemplate.postForObject(PROJECT_CREATE_REST_URL, request, String.class);
    }

    @Test
    public void createProject() throws IOException {
        @NotNull final JsonNode root = objectMapper.readTree(projectResultAsJson);
        assertNotNull(projectResultAsJson);
        assertNotNull(root);
        assertNotNull(root.path("name").asText());
        assertEquals(root.path("name").asText(), projectName);
        assertNotNull(root.path("id").asText());
        projectId = root.path("id").asText();
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("id", projectId);
        @NotNull final HttpEntity<?> request = new HttpEntity<Object>(requestBody, header);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject(PROJECT_DELETE_REST_URL, request, String.class);
    }
}
