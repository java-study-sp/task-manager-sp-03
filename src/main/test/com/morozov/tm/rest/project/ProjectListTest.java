package com.morozov.tm.rest.project;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;

public class ProjectListTest {
    @NotNull private final static String PROJECT_LIST_REST_URL = "http://localhost:8080/restprojects/list";
    @NotNull private final ObjectMapper objectMapper = new ObjectMapper();
    @Nullable private String projectResultAsJson;

    @Before
    public void setUp() throws Exception {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        projectResultAsJson = restTemplate.getForObject(PROJECT_LIST_REST_URL, String.class);
    }

    @Test
    public void projectList() throws IOException {
        @NotNull final JsonNode root = objectMapper.readTree(projectResultAsJson);
        assertNotNull(projectResultAsJson);
        assertNotNull(root);
        if (root.isArray()) {
            for (@NotNull final JsonNode projectNode : root) {
                assertNotNull(projectNode.path("name").asText());
                assertNotNull(projectNode.path("id").asText());
            }
        }
    }
}
