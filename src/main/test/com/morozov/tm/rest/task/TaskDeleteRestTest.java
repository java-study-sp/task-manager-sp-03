package com.morozov.tm.rest.task;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;

public class TaskDeleteRestTest {
    @NotNull private static final String PROJECT_CREATE_REST_URL = "http://localhost:8080/restprojects/create";
    @NotNull private static final String PROJECT_DELETE_REST_URL = "http://localhost:8080/restprojects/delete";
    @NotNull private static final String TASK_CREATE_REST_URL = "http://localhost:8080/resttasks/create";
    @NotNull private static final String TASK_DELETE_REST_URL = "http://localhost:8080/resttasks/delete";
    @NotNull private final ObjectMapper objectMapper = new ObjectMapper();
    @Nullable private String projectId;
    @Nullable private String taskId;

    @Before
    public void setUp() throws Exception {
        @NotNull final HttpHeaders headerProjectCreate = new HttpHeaders();
        headerProjectCreate.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBodyProjectcreate = new LinkedMultiValueMap<>();
        @NotNull final String projectName = "Test Project";
        requestBodyProjectcreate.add("projectName", projectName);
        @NotNull final HttpEntity<?> request = new HttpEntity<Object>(requestBodyProjectcreate, headerProjectCreate);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @Nullable final String projectResultAsJson = restTemplate.postForObject(PROJECT_CREATE_REST_URL, request, String.class);
        @NotNull final JsonNode root = objectMapper.readTree(projectResultAsJson);
        projectId = root.path("id").asText();
        @NotNull final HttpHeaders headerTaskCreate = new HttpHeaders();
        headerTaskCreate.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBodyTaskCreate = new LinkedMultiValueMap<>();
        @NotNull final String taskName = "Test Task";
        requestBodyTaskCreate.add("taskName", taskName);
        requestBodyTaskCreate.add("projectId", projectId);
        @NotNull final HttpEntity<?> requestTaskCreate = new HttpEntity<Object>(requestBodyTaskCreate, headerTaskCreate);
        @Nullable final String taskResultAsJson = restTemplate.postForObject(TASK_CREATE_REST_URL, requestTaskCreate, String.class);
        @NotNull final JsonNode rootTaskCreate = objectMapper.readTree(taskResultAsJson);
        taskId = rootTaskCreate.path("id").asText();
    }

    @Test
    public void deleteTask() {
        @NotNull final HttpHeaders headerTask = new HttpHeaders();
        headerTask.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestTaskBody = new LinkedMultiValueMap<>();
        requestTaskBody.add("id", taskId);
        @NotNull final HttpEntity<?> requestTask = new HttpEntity<Object>(requestTaskBody, headerTask);
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final ResponseEntity responseEntity = restTemplate.exchange(TASK_DELETE_REST_URL, HttpMethod.POST, requestTask, String.class);
        @NotNull final HttpStatus status = responseEntity.getStatusCode();
        assertEquals(status, HttpStatus.OK);
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        @NotNull final MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("id", projectId);
        @NotNull final HttpEntity<?> request = new HttpEntity<Object>(requestBody, header);
        restTemplate.postForObject(PROJECT_DELETE_REST_URL, request, String.class);
    }
}
