package com.morozov.tm.rest.task;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;

public class TaskListTest {
    @NotNull private final static String TASK_LIST_REST_URL = "http://localhost:8080/resttasks/list";
    @NotNull private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void taskList() throws IOException {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @Nullable String taskResultAsJson = restTemplate.getForObject(TASK_LIST_REST_URL,String.class);
        @NotNull final JsonNode root = objectMapper.readTree(taskResultAsJson);
        assertNotNull(taskResultAsJson);
        assertNotNull(root);
        if (root.isArray()) {
            for (@NotNull final JsonNode projectNode : root) {
                assertNotNull(projectNode.path("name").asText());
                assertNotNull(projectNode.path("id").asText());
            }
        }
    }
}
